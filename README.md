ghost-backup is a simple script to backup your ghost blog.

It tries to be smart and to save space on your disk:

- database is backed-up each time whatsoever
- the rest of the content is discarded if there was no change
  since last backup

You can configure how many *database backups* you want to keep,
and how many *content backups* you want yo keep.

To enable automatic backups with cron, add such a line to
your crontabs:

	@daily /usr/local/bin/ghost-backup

Logs end up in syslog, or in the console if you run manually.


